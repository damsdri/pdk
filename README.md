This is a very basic completion script for the Puppet Development Kit command line tool.

I wrote this while learning the PDK tool and ZSH completion scripting.

It is far from the most advanced, nor is it the most clean completion code. But it gets the job done.

All feedback and improvements on this are welcome.